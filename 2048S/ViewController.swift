//
//  ViewController.swift
//  2048S
//
//  Created by Ciaran Slattery on 1/30/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    @IBAction func startGameButtonTapped(sender: UIButton)
    {
        let game = GameViewController(dimension: 8, threshold: 2048)
        self.presentedViewController(game, animated: true, completion: nil)
    }

}

