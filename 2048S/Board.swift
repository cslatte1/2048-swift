//
//  Board.swift
//  2048S
//
//  Created by Ciaran Slattery on 1/30/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

import UIKit

class Tile: UIView {
    
    var delegate: AppearanceProtocol
    var value: Int = 0 {
        didSet {
            backgroundColor = delegate.tileColor(value)
            numberLabel.textColor = delegate.numberColor(value)
            numberLabel.text = "\(value)"
        }
        var numberLabel: UILabel
        
        required init(coder: NSCoder) {
        fatalError("NSCoding not supported")
        }
        
        init(position: CGPoint, width: CGFloat, value: Int, radius: CGFloat, delegate d: AppearanceProtocol) {
        addSubview(numberLabel);
        layer.cornerRadius = radius
        
        self.value = value
        backgroundColo = delegate.tileColor(value)
        numberLabel.textColor = delegate.numberColor(value)
        numberLabel.text = "\(value)"
        }
    }
}